import requests
import sqlite3
import datetime
from time import sleep

conn = sqlite3.connect('logging.db')
c = conn.cursor()
c.execute('''create table if not exists logs
             (id decimal(10,5) primary key, sonuc integer, tarih timestamp, ping)''')
conn.commit()
conn.close()

url = 'https://kap2.uozturk.com'
try:
    while True:
        r = requests.get(url)
        conn = sqlite3.connect('logging.db')
        c = conn.cursor()
        now = datetime.datetime.now()
        print(r.status_code,end=" ", flush=True)
        print(now,end=" Geçen Zaman:", flush=True)
        print(r.elapsed.total_seconds(),end=" Toplam Kayit:", flush=True)
        c.execute("INSERT INTO logs(sonuc,tarih,ping) VALUES ('200', ?,?)", (now,r.elapsed.total_seconds(),))
        c.execute("select count(sonuc) as counts, Avg(ping) as avgs from logs group by sonuc having sonuc = 200")
        gelen = c.fetchall()
        print(gelen[0][0],end=" Avgs:", flush=True)
        print(gelen[0][1])
        conn.commit()
        conn.close()
        sleep(30)

except requests.exceptions.Timeout:
    print('Timeout')
    conn = sqlite3.connect('logging.db')
    c = conn.cursor()
    now = datetime.datetime.now()
    c.execute("INSERT INTO logs(sonuc,tarih,ping) VALUES ('timeout', ?)", (now,r.elapsed.total_seconds(),))
    conn.commit()
    conn.close()
except requests.exceptions.ConnectionError as e:
    print(e)
except requests.exceptions.RequestException as e:
    print(e)
    conn = sqlite3.connect('logging.db')
    c = conn.cursor()
    now = datetime.datetime.now()
    c.execute("INSERT INTO logs(sonuc,tarih,ping)  VALUES (?, ?)", (e,now,r.elapsed.total_seconds(),))
    conn.commit()
    conn.close()